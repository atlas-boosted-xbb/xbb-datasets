#!/usr/bin/env bash

for DSID in 3610{20..32} 301{488..507} 3013{22..35} 3012{54..87}; do
    rucio ls --short mc16_13TeV:mc16_13TeV.$DSID*.AOD.*
done
