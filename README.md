Training and performance datasets
=================================

This repository tracks various files used for studies in FTAG5
derivations. See the request in [ATLFTAGDPD-121][0] and cloned
tickets. The data was processed using the
[`training-dataset-dumper`][2].

If you have problems let Dan (dguest@cern.ch) know.


Shortened names
---------------

The `input-names.txt` file came out of [pandamonium][1]. You can get
them with something like

```bash
pandamon -u '' user.dguest.*hbbTraining*.p3/ -s IN
```

I ran this to shorten the dataset names
```
cat input-names.txt | sed -r -e 's:[^.]*r9315[^.]*:mc16a:' -e 's:[^.]*r10210[^.]*:mc16d:' -e 's:[^.]*(Pythia|Pow)([^_]+_){2}::' | awk -F . '{print $6"."$3"."$2}' > short-names.txt
```

If you want "nicer" names, you can run the `rename.py` script. See the
options via `rename.py --help`.


[0]: https://its.cern.ch/jira/browse/ATLFTAGDPD-121
[1]: https://github.com/dguest/pandamonium
[2]: https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper
